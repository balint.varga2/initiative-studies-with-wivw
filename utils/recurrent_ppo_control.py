import torch
import numpy as np
import os
from sb3_contrib import RecurrentPPO
from utils.controller.vanilla_mpc import vanilla_mpc


class RecurrentPPOAgent:
    def __init__(self):

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model_weights_path = os.getenv("HOME") + '/MasterThesis_ws/src/dongxu-master-thesis/reinforcement_learning/RecurrentPPO/sb3/recurrent_ppo_policy_v1'
        self.agent = RecurrentPPO.load(self.model_weights_path)
        self.lstm_state = None
        self.episode_starts = np.ones((1,), dtype=bool)
        self.last_obs = None

        self.safe_distance_y = 1.5

    def reset(self):
        self.lstm_state = None
        self.episode_starts = np.ones((1,), dtype=bool)

    def eval(self, ped, veh):

        is_ped_outside_road = abs(veh.y_var - ped.y_var) > self.safe_distance_y
        is_veh_passed = veh.x_var > ped.x_var
        is_ped_passed = False
        if (ped.y_var - veh.y_var) * ped.yspeed_ms_var > 0 and is_ped_outside_road:
            is_ped_passed = True

        # if pedestrian or vehicle has passed
        if is_ped_passed or is_veh_passed:
            return vanilla_mpc(ped, veh)

        obs = np.array([ped.x_var - veh.x_var,
                        ped.y_var - veh.y_var,
                        ped.xspeed_ms_var,
                        ped.yspeed_ms_var,
                        veh.xspeed_ms_var,
                        ped.intention_var])
        if self.last_obs is not None and self.last_obs[0] < obs[0] and obs[0] > 10.0:
            self.reset()
        self.last_obs = obs

        veh_acc_cmd_ms2, self.lstm_state = self.agent.predict(obs, state=self.lstm_state, episode_start=self.episode_starts, deterministic=True)
        self.episode_starts = False
        veh_acc_cmd_ms2 = float(veh_acc_cmd_ms2)
        pub_text = "I'm stopping" if veh_acc_cmd_ms2 < 0 else "I'm driving"

        return veh_acc_cmd_ms2, pub_text



