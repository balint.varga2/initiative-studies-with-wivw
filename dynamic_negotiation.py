from utils.non_linear_mpc_explicit_2d import NonLinearMPC_explicit # YES
from utils.rule_based_controller import RuleBasedController # YES
from utils.human_lead_controller import HumanLeadController
# Human lead function -- there is no negotiation
from utils.sim_model_objects import Object
import time



class parameter_helper:
    def __init__(self, val):
        self.value = val

class DynamicNegotiation():

    def __init__(self, con_name):
        
        # define parameters
        self.declare_parameters(
            namespace='',
            parameters=[
                ('controller', con_name),
            ])

        # ===== Model States ===== #
        self.ped = Object()
        self.ped.yspeed_ms_var = 0.0
        self.ped.x_var = 0.0
        self.ped.y_var = 0.0
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.0
        self.ped.intention_var = 0.0  # between 0 and 1

        self.veh = Object()
        self.veh.xspeed_ms_var = 0.0
        self.veh.x_var = 0.0
        self.veh.y_var = 0.0
        self.veh.xspeed_ref_para = 5.0
        self.veh.xspeed_max_para = 10.0
        self.veh.road_width = 1.65
        self.veh.x_where_discount_starts = 15

        self.select_controller(con_name)
        # self.controller_name = 'human_lead'
        # self.controller = HumanLeadController()
        
        
        self.is_discounting_intention = True
        self.count_para = 0.0
        

        # ===== Published Information ===== #
        self.veh_acc_cmd_ms2 = 0.0
        self.pub_text = ""


        #print(self.__dict__)

    #Addendum Thomas Start

    def declare_parameters(self, *args, **kwargs):
      
        for key, value in kwargs.items():
            print(key)
            print(value)
            if key == "parameters":
                for parameter in value:
                    name, val = parameter
                    end_val = parameter_helper(val)
                    self.__dict__[name] = end_val


    def get_parameter(self, name): 
        return self.__dict__[name]

    ##Addendum Thomas End


    def select_controller(self, controller_name):
        # TODO: choosing three controller for the study
        self.controller_name = controller_name
        if self.controller_name == 'non_linear_mpc_explicit':
            self.controller = NonLinearMPC_explicit()

        elif self.controller_name == 'rule_based':
            self.controller = RuleBasedController()

        # TODO: Adding human lead control strategy
        elif self.controller_name == 'human_lead':
            self.controller = HumanLeadController()
        else:
            self.get_logger().info('This controller is invalid, set controller as default mode: MPC')
            self.controller = NonLinearMPC_explicit()

    def intention_discounting(self):
        # WARNING
        # TODO: hard coded parameters
        is_discounting = self.veh.road_width/2 < abs(self.ped.y_var - self.veh.y_var) < 5 and abs(self.ped.yspeed_ms_var) < 1.0 \
            and abs(self.ped.x_var - self.veh.x_var) < self.veh.x_where_discount_starts # Distance from the pedestrain!
        
        #print("PED y pos:", self.ped.y_var)
        #print("VEH y pos:", self.veh.y_var)
        if is_discounting:
            self.count_para += 0.33
        else:
            self.count_para = 0.0
        self.ped.intention_var *= pow(0.95, self.count_para * 0.5)


    # cyclic timer function 
    def run_negotiation(self):
        
        if self.is_discounting_intention:
            self.intention_discounting()

        self.veh_acc_cmd_ms2, self.pub_text = self.controller.eval(self.ped, self.veh)


        return self.pub_text, self.veh_acc_cmd_ms2

