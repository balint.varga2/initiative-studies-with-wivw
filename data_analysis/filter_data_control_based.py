import os

# Define the directory to save the split files


def filter_data(test_subj_num):
    test_subj_num = str(test_subj_num) if test_subj_num>9 else "0"+str(test_subj_num)
    num_data_points = 200000
    file = "/home/varga/wivw_paper/initiative-studies-with-wivw/data_analysis/study_data/"+test_subj_num+".1.asc"

    test_sub_number = file.split('/')[-1][0:2]

    output_directory = "/home/varga/wivw_paper/initiative-studies-with-wivw/data_analysis/split_data/"+test_sub_number
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    # Initialize a dictionary to store data points for each group
    data_groups = {i: [] for i in range(1, 7)}
    group_control = {1: [], 2: [], 3: []}
    id_of_data = 0
    prev_vehicle_y_value = None
    prev_group_control_id = None

    group_number = 0
    with open(file, 'r') as infile:
        for line_number, line in enumerate(infile):
            # Skip the first line if it's a header line
            data_array = line.strip().split(',')

            if line_number == 0:
                id_of_data = data_array.index("Vehicle.Y")
                id_of_start_stop = data_array.index("Switchbox_start_1")
                id_of_data_control_num = data_array.index("Switchbox.repetitions")
                first_line = line
                print(id_of_data_control_num)
                continue
            
            try:
                vehicle_y_value = float(data_array[id_of_data])
                start_stop_value = float(data_array[id_of_start_stop])
                
                group_control_id = data_array[id_of_data_control_num]
                group_control_id = group_control_id.strip().split('.')
                group_control_id = int(group_control_id[0])
            except ValueError:
                print("Skipping line due to invalid Vehicle.Y value:", line)
                continue  # Skip lines where Vehicle.Y value cannot be converted to float
            #print(vehicle_y_value)


            # Groupping based on the scenarios
            if line_number>100 and \
                prev_start_stop_value<0.1 and \
                start_stop_value>0.9:
                group_number = group_number+1
                data_groups[group_number].append(first_line)
            
            if not group_number == 0:
            # Store the current line in the corresponding group
                data_groups[group_number].append(line)
                
            # Controller based groupping
            if line_number>100 and \
                not group_control_id== prev_group_control_id and \
                len(group_control[group_control_id]) == 0:
                group_control[group_control_id].append(first_line)
                print("first line is added")

            if not group_control_id==0:
                group_control[group_control_id].append(line)
            

            # Update previous Vehicle.Y value for the next iteration
            prev_start_stop_value = start_stop_value
            prev_group_control_id = group_control_id

    # Write data to separate files for each group
    #for group_number, data_points in data_groups.items():
    #    output_file = os.path.join(output_directory, f"group_{group_number}.asc")
    #    with open(output_file, 'w') as outfile:
    #        for data_point in data_points:
    #            outfile.write(data_point)

    for group_number, data_points in group_control.items():
        output_file = os.path.join(output_directory, f"control_group_{group_number}.asc")
        with open(output_file, 'w') as outfile:
            for data_point in data_points:
                outfile.write(data_point)

    print("Split files created successfully.")

for i in range(1,26):
    filter_data(i)

print("Finished")