import torch
import numpy as np
import os
from stable_baselines3 import PPO
from utils.controller.vanilla_mpc import vanilla_mpc

is_with_intention = True


class PPOAgent:
    def __init__(self):

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model_weights_path = os.getenv("HOME") + '/MasterThesis_ws/src/dongxu-master-thesis/reinforcement_learning/PPO/sb3/ppo_v1_local'
        self.agent = PPO.load(self.model_weights_path)

        self.safe_distance_y = 1.5

    def eval(self, ped, veh):

        is_ped_outside_road = abs(veh.y_var - ped.y_var) > self.safe_distance_y
        is_veh_passed = veh.x_var > ped.x_var
        is_ped_passed = False
        if (ped.y_var - veh.y_var) * ped.yspeed_ms_var > 0 and is_ped_outside_road:
            is_ped_passed = True

        # if pedestrian or vehicle has passed
        if is_veh_passed or is_ped_passed:
            # self.model_weights_path = os.getenv(
            #     "HOME") + '/MasterThesis_ws/src/dongxu-master-thesis/reinforcement_learning/PPO/sb3/ppo_policy_vanilla'
            # self.agent = PPO.load(self.model_weights_path)
            return vanilla_mpc(ped, veh)
        # else:
        #     self.model_weights_path = os.getenv(
        #         "HOME") + '/MasterThesis_ws/src/dongxu-master-thesis/reinforcement_learning/PPO/sb3/ppo_policy_test'
        #     self.agent = PPO.load(self.model_weights_path)
        if is_with_intention:
            states = np.array([ped.x_var - veh.x_var,
                               ped.y_var - veh.y_var,
                               ped.xspeed_ms_var,
                               ped.yspeed_ms_var,
                               veh.xspeed_ms_var,
                               ped.intention_var])
        else:
            states = np.array([ped.x_var - veh.x_var,
                               ped.y_var - veh.y_var,
                               ped.xspeed_ms_var,
                               ped.yspeed_ms_var,
                               veh.xspeed_ms_var])
        veh_acc_cmd_ms2 = self.agent.predict(states, deterministic=True)[0]
        veh_acc_cmd_ms2 = float(veh_acc_cmd_ms2)
        pub_text = "I'm stopping" if veh_acc_cmd_ms2 < 0 else "I'm driving"

        return veh_acc_cmd_ms2, pub_text



