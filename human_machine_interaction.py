from silab_ppu import *
from silab_sys import *
from dynamic_negotiation import DynamicNegotiation
#from model_pedcitive_negotiation import NonLinearMPC_explicit

"""
This programm implements a intention-aware decision making of a autonomous vehicle
The implementation suits for testing with the pedestrain simulator of WIVW

authors: 
Balint Varga, IRS/KIT, balint.varga2@kit.edu
Thomas Brand, WIVW, brand@wivw.de
"""

class human_machine_interaction(PPU):
	
    def __init__(self):
        """ The PPU's constructor.
    
        Every derived PPU class must implement a constructor. Within the
        constructor, the functions self.var_in() and self.var_out() may be
        used to create additional SILAB variables.
        """
        super().__init__()
        log_sys("This file was generated using SILABDPUWizard!")
        
        # Inputs from SILAB
        self.cross_point_y = 4.1
        self.cross_point_x = 0

        self.ped_x  = self.var_in("ped_x", 0)
        self.ped_y  = self.var_in("ped_y", 0)
        self.ped_vx = self.var_in("ped_vx", 0)
        self.ped_vy = self.var_in("ped_vy", 0)
        self.ped_intention = self.var_in("ped_intention", 0)

        self.veh_x  = self.var_in("veh_x", 0)
        self.veh_y  = self.var_in("veh_y", 0)
        self.veh_vx = self.var_in("veh_vx", 0)
        self.veh_vy = self.var_in("veh_vy", 0)
        
        # Outputs
        self.text_msg = self.var_out("text_msg", "", type=PPUVt.PPU_STRING)
        self.veh_des_acc = self.var_out("veh_des_acc", 0)

        self.target_speed = self.var_out("target_speed", 0)

        #self.negotiations_array = ["non_linear_mpc_explicit", "rule_based", "human_lead"]
        self.negotiation_type = "non_linear_mpc_explicit"
    
        self.negotiation_type = self.var_out("negotation_type", self.negotiation_type, type=PPUVt.PPU_STRING)
        # define object of the dynamic negotiation,
        # self.dynamic_negotiation_silab = NonLinearMPC_explicit
        self.dynamic_negotiation_silab = DynamicNegotiation(str(self.negotation_type))
        
        
        # Addding x offset for the proper distance
        # The reason for that are the differences between the IRS and WIVW Simulator position definition
        self.vehicle_offset = 4
        


        log_sys("INIT DONE")
        
        # iteration over the three decision making strategies
        #Dynamic Negotiation Values
        for key, value in self.dynamic_negotiation_silab.__dict__.items():
            if type(value) == float: 
                self.var_out(f"DN.{key}", value)
            
            if key == "ped":
                for key2, value2 in value.__dict__.items(): 
                    if type(value2) == float: 
                        self.var_out(f"DN.PED.{key2}", value2)
            elif key == "veh":
                for key2, value2 in value.__dict__.items(): 
                    if type(value2) == float: 
                        self.var_out(f"DN.VEH.{key2}", value2)
            elif key == "controller":
                for key2, value2 in value.__dict__.items(): 
                    if type(value2) == float: 
                        self.var_out(f"DN.CON.{key2}", value2)



       #[self.var_out(f"DN_{key}", value) for key, value in self.dynamic_negotiation_silab.__dict__.items() if type(value) == float ]
        log_sys(str(self.dynamic_negotiation_silab.__dict__.items()))


    def prepare(self) -> bool:
        """ Called once when loading the simulation.
        Task: Run preparations that might take a long time, such as
        reading configuration files, precomputing tables, etc.
        return true if preparations succeeded, else false
        """
        log_sys("PREPARE DONE")

        return True

    def start(self, i_step) -> PPUReady:
        """ Called periodically once the user clicks 'Launch'
        Task: Run preparations that must be done whenever the simulation
        is re-started. The task must be subdivided into small steps
        (approx. 1ms). The method will be called repeatedly as long as
        PPU_NOTREADY is returned.
        """       

        return PPUReady.PPU_READY

    def trigger(self, i_time, i_time_error):
        """ Called periodically while the simulation is running.
        Task: Implement the PPU's functionality during the simulation.
        Each call shouldn't take more than 1ms to complete.
        """

        # Function IRS
        # with 20 Hz or 25 Hz
        # update
        
        self.dynamic_negotiation_silab.ped.x_var                  = self.ped_x
        self.dynamic_negotiation_silab.ped.y_var                  = -self.ped_y
        self.dynamic_negotiation_silab.ped.yspeed_ms_var          = -self.ped_vy
        self.dynamic_negotiation_silab.ped.intention_var          = self.ped_intention


        # Addding x offset for the proper distance
                
        self.dynamic_negotiation_silab.veh.xspeed_ms_var = self.veh_vx
        self.dynamic_negotiation_silab.veh.x_var         = self.veh_x + self.vehicle_offset
        self.dynamic_negotiation_silab.veh.y_var         = -self.veh_y

        self.text_msg, self.veh_des_acc = self.dynamic_negotiation_silab.run_negotiation()

        self.target_speed = self.veh_vx + self.veh_des_acc

        #Updating of Dynamic_negotiation_silab values: 
        for key, value in self.dynamic_negotiation_silab.__dict__.items():
            self.__dict__[f"DN.{key}"] = value


        #Updating Items: 
        #Dynamic Negotiation Values
        for key, value in self.dynamic_negotiation_silab.__dict__.items():
            if type(value) == float: 
                self.__dict__[f"DN.{key}"] = value
            
            if key == "ped":
                for key2, value2 in value.__dict__.items(): 
                    if type(value2) == float: 
                        self.__dict__[f"DN.PED.{key2}"] = value2
            elif key == "veh":
                for key2, value2 in value.__dict__.items(): 
                    if type(value2) == float: 
                        self.__dict__[f"DN.VEH.{key2}"] = value2
            elif key == "controller":
                for key2, value2 in value.__dict__.items(): 
                    if type(value2) == float: 
                        self.__dict__[f"DN.CON.{key2}"] = value2
        return

    def pause(self):
        """ Is called once when the user clicks 'Pause'.
        Task: Preparations for pausing the simulation.
        """
        return

    def resume(self):
        """ Called once when the simulation is resumed.
        Task: Preparations that have to be done when the simulation is resumed.
        This function should not take longer than 1ms to complete.
        """
        return

    def stop(self, step) -> PPUReady:
        """ Called periodically once the user clicks 'Stop' or when one
        PPUs preparation have failed.
        Task: Cleanup things before the simulation is stopped. This
        function is clled repeatedly as long as PPU_NOTREADY is returned.
        Each call shouldn't take more than 1ms to complete.
        """
        return PPUReady.PPU_READY
    
    # IRS functions

    def is_ped_close(self):
        # Current implementation: 
        # vehicle travels in x direction
        # Pedestrian moves in y directio
        is_close = (x_ped - x_veh)**2


        return is_close


    def __del__(self):
        """  The PPU's destructor.
        The method will be called when the simulation is shut down, no 
        matter if the shutdown is regular or using the "emergency stop".
        Warning: May be called more than once.
        Warning: May be called in any state of the simulation.
        Task: Release all resources of the PPU.
        """
        super().__del__()

