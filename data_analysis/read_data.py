import numpy as np
from astropy.io import ascii

import matplotlib.pyplot as plt
import os


#data = ascii.read("test_data_2023-11-14.asc")
#data = ascii.read("C:/Users/ha4513/Desktop/Data-Initiative/06.1.asc")
def process_data_file(file_path):
    counter = 0
    num_data_points = 250000
    ped_y_pos = np.zeros(num_data_points)
    time = np.zeros(num_data_points)

    with open(file_path, 'r') as infile:
        for line in infile:
            data_array = line.strip().split(',')
            if counter == 0:
                id_of_data = data_array.index("Vehicle.Y")
                id_of_time = data_array.index("Messzeitpunkt")
            else:
                ped_y_pos[counter] = float(data_array[id_of_data])
                time[counter] = float(data_array[id_of_time])
                
            counter += 1

            if counter > num_data_points-1:
                break

    return ped_y_pos, time

folder_path = "/home/varga/wivw_paper/initiative-studies-with-wivw/data_analysis/split_data/02/"

# Get a list of all files in the directory
files = os.listdir(folder_path)

iter = 0
time = {0: [], 1: [], 2: []}
ped_y_pos = {0: [], 1: [], 1: []}

for file in files:
    file = folder_path + file

    
    ped_y_pos[iter], time[iter] = process_data_file(file)
    print(iter)
    iter = iter+1


# Plotting
plt.plot(time[0], ped_y_pos[0], color='green', label='File 3 - Vehicle.Y')
plt.plot(time[1], ped_y_pos[1], color='blue', label='File 1 - Vehicle.Y')
plt.plot(time[2], ped_y_pos[2], color='red', label='File 2 - Vehicle.Y')
plt.xlabel('Time')
plt.ylabel('Vehicle.Y Position')
plt.title('Vehicle.Y Position vs. Time')
plt.grid(True)
plt.legend()
plt.show()




#print(headers)
#print(data.columns)
#plt.plot(ped_y_pos)
#plt.plot(time[2:50000], ped_y_pos[2:50000])
#plt.show()
# Messzeitpunkt




